const fs = require('fs').promises;

const fileName = 'items.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },

    async getItem() {
        return data;
    },

    async getById(id) {

        const item = data.find(ob => {
            if (ob.id === id) {
                return ob;
            }
        });

        return item;
    },

    async delete(id) {
        let fl = 0;

        data.forEach((ob, index) => {
            if (ob.id === id) {
                data.splice(index, 1);
                fl++;
            }
        });

        if (fl > 0) {
            await this.save();
            return ({message: 'item was deleted'});
        } else {
            return ({error: 'no such item'});
        }

    },

    async addItem(item) {
        data.push(item);
        await this.save();
    },

    async save() {
        await fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
};