const fs = require('fs').promises;

const fileName = 'places.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },

    async getPlace() {
        return data;
    },

    async getById(id) {

        const place = data.find(ob => {
            if (ob.id === id) {
                return ob;
            }
        });

        return place;
    },

    async delete(id) {
        let fl = 0;

        data.forEach((ob, index) => {
            if (ob.id === id) {
                data.splice(index, 1);
                fl++;
            }
        });

        if (fl > 0) {
            await this.save();
            return ({message: 'place was deleted'});
        } else {
            return ({error: 'no such place'});
        }

    },

    async addPlace(place) {
        data.push(place);
        await this.save();
    },

    async save() {
        await fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
};