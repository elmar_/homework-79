const fs = require('fs').promises;

const fileName = 'categories.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },

    async getCategory() {
        return data;
    },

    async getById(id) {

        const category = data.find(ob => {
            if (ob.id === id) {
                return ob;
            }
        });

        return category;
    },

    async delete(id) {
        let fl = 0;

        data.forEach((ob, index) => {
            if (ob.id === id) {
                data.splice(index, 1);
                fl++;
            }
        });

        if (fl > 0) {
            await this.save();
            return ({message: 'category was deleted'});
        } else {
            return ({error: 'no such category'});
        }

    },

    async addCategory(category) {
        data.push(category);
        await this.save();
    },

    async save() {
        await fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
};