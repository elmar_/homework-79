const express = require('express');
const {nanoid} = require('nanoid');
const path = require('path');
const router = express.Router();
const multer = require('multer');
const config = require('../config');

const fileWork = require('../fileSystem/items_file');
const filePlace = require('../fileSystem/places_file');
const fileCategory = require('../fileSystem/categories_file');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});


router.post('/', upload.single('image'), async (req, res) => {
    const item = req.body;
    let flCategory = 0;
    let flPlaces = 0;

    if (req.file) {
        item.image = req.file.filename;
    }

    if (!item.name) {
        console.log('no name');
        return res.send({error: 'enter name please'});
    }

    if (!item.description) {
        item.description = '';
    }

    if (!item.idCategory) {
        return res.send({error: 'enter category id (ключь: idCategory)'})
    }

    if (!item.idPlace) {
        return res.send({error: 'enter place id (ключь: idPlace)'})
    }

    const places = await filePlace.getPlace();
    places.forEach(place => {
        if (place.id === req.body.idPlace) {
            flPlaces ++;
        }
    });

    if (flPlaces === 0) {
        return res.send({error: 'no such id in places'});
    }

    const categories = await fileCategory.getCategory();
    categories.forEach(category => {
        if (category.id === req.body.idCategory) {
            flCategory ++;
        }
    });

    if (flCategory === 0) {
        return res.send({error: 'no such id in category'});
    }

    item.id = nanoid();
    await fileWork.addItem(item);
    res.send(item);
});

router.get('/', async (req, res) => {
    const data = await fileWork.getItem();

    const newData = data.map(ob => (
        {
            id: ob.id,
            name: ob.name
        }
    ));

    res.send(newData);
});

router.get('/:id', async (req, res) => {

    const item = await fileWork.getById(req.params.id);

    if (!item) {
        return res.send({error: 'no such item id'});
    }


    res.send(item);
});

router.delete('/:id', async (req, res) => {

    const response = await fileWork.delete(req.params.id);

    res.send(response);
});

module.exports = router;