const express = require('express');
const {nanoid} = require('nanoid');
const router = express.Router();

const fileWork = require('../fileSystem/categories_file');
const fileItem = require('../fileSystem/items_file');


router.post('/', async (req, res) => {
    const category = req.body;

    if (!category.name) {
        console.log('no name');
        return res.send({error: 'enter name please'});
    }

    if (!category.description) {
        category.description = '';
    }

    category.id = nanoid();
    await fileWork.addCategory(category);
    res.send(category);
});

router.get('/', async (req, res) => {
    const data = await fileWork.getCategory();

    const newData = data.map(ob => (
        {
            id: ob.id,
            name: ob.name
        }
    ));

    res.send(newData);
});

router.get('/:id', async (req, res) => {

    const category = await fileWork.getById(req.params.id);

    if (!category) {
        return res.send({error: 'no such category id'});
    }


    res.send(category);
});

router.delete('/:id', async (req, res) => {

    const id = req.params.id;

    let fl = 0;

    const items = await fileItem.getItem();

    items.forEach(item => {
        if (item.idCategory === id) {
            fl ++;
        }
    });

    if (fl > 0) {
        res.send({error: 'нельзя удалять, так как есть связанный item'});
    } else {
        const response = await fileWork.delete(id);
        res.send(response);
    }
});

module.exports = router;