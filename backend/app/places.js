const express = require('express');
const {nanoid} = require('nanoid');
const router = express.Router();

const fileWork = require('../fileSystem/places_file');
const fileItem = require('../fileSystem/items_file');


router.post('/', async (req, res) => {
    const place = req.body;

    if (!place.name) {
        console.log('no name');
        return res.send({error: 'enter name please'});
    }

    if (!place.description) {
        place.description = '';
    }

    place.id = nanoid();
    await fileWork.addPlace(place);
    res.send(place);
});

router.get('/', async (req, res) => {
    const data = await fileWork.getPlace();

    const newData = data.map(ob => (
        {
            id: ob.id,
            name: ob.name
        }
    ));

    res.send(newData);
});

router.get('/:id', async (req, res) => {

    const place = await fileWork.getById(req.params.id);

    if (!place) {
        return res.send({error: 'no such place id'});
    }


    res.send(place);
});

router.delete('/:id', async (req, res) => {
    const id = req.params.id;

    let fl = 0;

    const items = await fileItem.getItem();

    items.forEach(item => {
        if (item.idPlace === id) {
            fl ++;
        }
    });

    if (fl > 0) {
        res.send({error: 'нельзя удалять так как есть связанный item'});
    } else {
        const response = await fileWork.delete(id);
        res.send(response);
    }


});

module.exports = router;