const express = require('express');
const cors = require('cors');

const categories = require('./app/categories');
const places = require('./app/places');
const items = require('./app/items');

const categories_file = require('./fileSystem/categories_file');
const places_file = require('./fileSystem/places_file');
const items_file = require('./fileSystem/items_file');

const app = express();
app.use(express.json());
app.use(express.static('public'));
app.use(cors());


const port = '8000';

app.use('/categories', categories);
app.use('/places', places);
app.use('/items', items);

const run = async () => {
    await categories_file.init();
    await places_file.init();
    await items_file.init();

    app.listen(port, () => {
        console.log('We are on port ' + port);
    });
};

run().catch(console.error);


